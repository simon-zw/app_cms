## 项目介绍
小组协作式Axure项目
中山大学南方学院 文学与传媒学院 网络与新媒体专业
- 成员：张威、郑梓轩、方闻熙、严诗婷、孔晓桐、陈雅慧、吴欣琳

### 产品方向
乘车购票类APP

### 产品定位
校园巴士线上乘车和远程巴士购票平台
### 产品核心目标
1. 打造流行新代步方式
2. 节约乘客候车时间/乘车时间
3. 给乘客和司机双方提供更多的实时信息
4. 规范交通秩序
5. 打造以中大南方为基点的交通网络一体化购票平台
### 产品核心功能
#### 小白联系方式：
- 建立一个校园叫车的“小白巴士”小程序，用户可以在小程序中选择乘车地点以及目的地去乘车。

#### 小白位置与乘客位置：
- 在小程序中设立即时地图功能，乘客可以看到小白的位置和运动方向，小白司机可以看到乘客的等待地点和意向目的地。

#### 小白即时乘客数量显示及预计：
- 有时候乘客等了挺久的小白巴士，终于等到了的却是满载的小白。
- 我们可以在等待界面中增加根据当前小白巴士乘坐数量，告知等待乘客的即时信息，让他们选择继续等待or其他方法通行。

#### 小白的通行轨迹
- 设立既定的交通路线，在通行较高峰期和高峰期按照交通路线行驶；
- 在假期、很早或者很晚的时候可以选择不按照路线交通按照智能规划路线/司机规划路线。（根据具体情况运作/会在小程序中通知）

#### 小白票价
- 智能安排票价：起步价为一元，校园内较远通行可为1.5元等，牌坊交通票价为2元。（用户更加接受，从而增加乘客）


#### 交通网络一体化购票平台
- 集合校巴，如约巴士、同乡会包车为一体，让用户在一个小程序中就可以解决校园巴士通行的问题。
### 目标市场
1. 在中大南方顺利推行，占据中大南方绝大多数用户。
2. 改善中大南方人们的通行思维和习惯，让用户更频繁的使用小白去通行。
3. 在中大南方试点取得好效果之后，将会扩大为适用于“小范围通行”的市场，例如社区，景区，游区等等场景。

### 竞品分析
#### 一、竞品分析对象
##### 腾讯乘车码
* 腾讯乘车码是一种可用来乘坐交通工具、适用于多个交通场景的二维码，是基于微信小程序开发的服务，2017年7月首次在广州上线。  
上线以来，腾讯乘车码已覆盖深圳、上海、广州、厦门等95个城市，支持BRT、公交、地铁、索道、轮渡等多个智慧交通移动支付场景。  
截至2018年9月22日，腾讯乘车码已覆盖北京、上海、广州、深圳、厦门、宁波、济南、梅州、大理等上百个城市，支持BRT、公交、地铁、索道、轮渡等智慧交通移动支付场景。未来，腾讯乘车码将持续推进与各地政府、交通集团的深度合作，推动城市交通进入高效、低碳的移动支付时代。   
![输入图片说明](https://images.gitee.com/uploads/images/2021/0109/125355_7779eb7c_2228515.png "6159252dd42a2834d9845ab056b5c9ea15cebf6f.png")  

#### 二、竞品目标用户
* 有出行需求的上班族、学生党等需要使用地铁或者公交出行的群体。
#### 三、竞品所解决的用户痛点
* 主要痛点就是追求更快更高效的完成充值、进站、出站。  
* 具体痛点：  
地铁卡/公交卡充值排队流程繁琐，耽误出行时间  
实物卡易丢失且丢失后造成金额损失  
钱包没有零钱或者忘记带钱包现金
忘记带地铁卡/公交卡  
地铁卡/公交卡余额不足  

#### 四、竞品市场分析

- 乘车码已经覆盖了北上广深、西安、重庆等超过110个城市，支持公交、地铁、轮渡等多种公共交通工具，据了解，腾讯乘车码的全国用户已经突破了6000万。
- 随着一线城市公共交通中轨道交通的客运量和比例迅速提高，会提高乘车码的使用量和需求。中国城市轨道交通协会发布的2019年度统计分析报告中显示2019广州地铁平均日客流量为906.8万人次，拿下了客运增量全国第一、单站最高日客运量全国第一等多个第一。这些数据都说明了乘坐地铁已经是市民的刚需和痛点。市场的扩大会带动产品的发展，而且乘车码作为腾讯旗下的产品，只能通过微信小程序使用，调用乘车码方便，随着扫码乘车的流行能够提高扫码乘车产品市场份额。

### 五、详细的竞品分析链接[链接地址](https://gitee.com/simon-zw/app_team/blob/master/%E7%AB%9E%E5%93%81%E5%88%86%E6%9E%90.md)

### 用户分析  
#### 1.用户需求分析
##### 小白巴士用户痛点

1. 用户无法得知小白具体位置，只能打电话预约，等待时间过长。
1. 小白没有具体、规范的行进路线，用户的乘车时间无法预测。
1. 携带大件行李可能无法乘车或收费过高。
1. 小白巴士的收费标准不合理。
1. 小白巴士的司机在驾驶过程中接打电话，易造成安全隐患。

#### 2.用户类型细分

##### 目标用户人群

- 高校的学生及教职工

小白巴士的主要使用人群，日常生活中对小白的需求较大。


##### 其他利益相关者

- 驾驶小白巴士的司机

主要负责接送乘客、小白日常的运营，每月收取固定工资。

- 开发人员

小程序或APP的开发人员，需要掌握一定技能知识，更新优化平台功能。从乘客乘车的后台搜集数据，进行整合获得数据价值。

- 校方

小白巴士的官方合作者，项目的投资方。该项目运行可以规范校园内的交通秩序，从小白的日常收益中获取利润。

- 小白维修工

小白车辆的维修人员，定期检测小白的零件安全性能。

- 广告商

赞助该项目的运营，在小程序或APP上投入广告获取收益。
#### 3.用户场景

 **场景一：** 用户出门前可在APP上查询小白的预计到站时间和预计下车时间，小白巴士固定的路线站点不一，用户只需步行一小段距离到小白即将到达的站点乘坐即可，此过程无需花费过多的时间步行到上车站点。规算好出门的时间，可以减少等待小白的时间，同时避免上下课迟到。

 **场景二：** 某些用户的宿舍离快递站/车站距离较远，需要搬运大件快递或携带大件行李时难度较大，小白改造后可存放大件行李，用户无需担忧行李的存放问题。用户可选择在快递站或车站的小白站点乘车。

 **场景三：** 在恶劣的天气下或者用户无法步行的情况下，乘坐小白代步可减轻一定的生理负担。

##### 用户画像

- 学生

![学生](https://images.gitee.com/uploads/images/2021/0110/174319_f3a834a2_2228721.png "小白巴士-学生.png")

- 老师

![老师](https://images.gitee.com/uploads/images/2021/0110/175212_b2d49428_2228721.png "老师.png")


